# Prérequis
- Node JS LTS 12.18.1
- lancer la commande ```npm install``` pour installer la library BigInt
# Utilisation
Cette application s'utilise dans une console (Linux, Powershell...). Pour la lancer 
il suffit de rentrer la commande ```npm run start``` ou ``node index.js``. 
L'application vous demandera de rentrer deux int et vous guidera dans le processus de cryptage et de décryptage.
(Si vous le souhaitez l'application propose des valeurs par défaut, 
pour les utiliser il suffit d'appuyer sur entrer sans rentrer de valeurs )
