import RSA from "./RSA.js";
import readline from "readline";

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
let rsa = new RSA();
rl.question("Quel est votre premier nombre premier et > 10 (p) ? (default 503) : ", function(p) {
    rl.question("Quel est votre deuxieme nombre premier > 10 (q) ? (default 563) : ", function(q) {
        let keys = rsa.generateKeys(parseInt(p) || 503, parseInt(q) || 563);
        console.log("Vos clefs sont :",keys);
        rl.question("Quel message voulez-vous crypter ? (default chocolat) : ", function(m) {
            let encryptedMessage = rsa.cryptMessage(m || "chocolat", keys.public);
            console.log("Votre message crypté est :", ...encryptedMessage);
            rl.question("Nous nous préparons maintenant a déchiffrer le message avec la private key, appuyer sur Entrée pour continuer... ", function() {
                let decryptedMessage = rsa.decryptMessage(encryptedMessage, keys.private);
                console.log("Votre message decrypté est : ", decryptedMessage);
                rl.close();
            });
        });
    });
});

rl.on("close", function() {
    console.log("BYE BYE !!!");
    process.exit(0);
});


