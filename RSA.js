import bigInt from "big-integer";

export default class RSA {

    constructor(){
    }
    // on calcule e sachant que p,q<e<φn et que PGCD(φn,e)=1
    calculate_e(p, q, fiN){
        let pluGrand = p > q ? p : q;
        let e;
        let valeursPossible = [];
        for (let i = pluGrand + 1; i < fiN; i++) {
            if (this.pgcd(fiN, i) === 1){
                valeursPossible.push(i);
            }
        }
        // on recupère une des valeur possible
        e = valeursPossible[Math.floor(Math.random()*valeursPossible.length)];
        return e
    }
    // une simple fonction qui sert a trouver le pgcd
    pgcd(a, b) {
        while (b>0) {
            let r=a%b;
            a=b;
            b=r;
        }
        return a;
    }

    // on calcule d sachant que e × d mod φn=1
    calculate_d(e, fiN, p, q){
        let d;
        let pluGrand = p > q ? p : q;
        let valeursPossible = [];
        for (let i = pluGrand + 1; i < fiN; i++) {
            if (e*i%fiN === 1){
                valeursPossible.push(i);
            }
        }
        // on recupère une des valeur possible
        d = valeursPossible[Math.floor(Math.random()*valeursPossible.length)];
        return d
    }

    // Generation des clef
    generateKeys(p, q){
        // on calcule n
        let n = p * q;
        // on calcule φ(𝑛)
        let fiN = (p-1) * (q-1);
        // on génère 𝑒 premier avec φ(𝑛)
        let e = this.calculate_e(p, q, fiN);
        // on genère d inverse de e modulo φ(𝑛)
        let d = this.calculate_d(e, fiN, p, q);
        let keys = {
            private : {
                n : n,
                d : d
            },
            public : {
                n : n,
                e : e
            }
        };
        return keys
    }

    // on converti le message en tableau de code ascii
    convertStringToAsciiTab(m){
        let asciiCodetab = [];
        for (let i = 0; i < m.length; i++){
            asciiCodetab.push(m.charCodeAt(i));
        }
        return asciiCodetab;
    }

    // on crypte le message carractère par carractère avec la formule B = A^e mob n
    // nous avons du utiliser la library bigInt car js ne support pas les nombres au dessus de 2^53 nativement
    cryptMessage(m, publicKey){
        let asciiCodetab = this.convertStringToAsciiTab(m);
        let encryptedMessage = [];
        for (let i = 0; i < asciiCodetab.length; i++){
            encryptedMessage.push(bigInt(asciiCodetab[i]).modPow(publicKey.e, publicKey.n));

        }
        encryptedMessage.forEach((cryptedascii, key)=>{
            encryptedMessage[key] = cryptedascii.toJSNumber();
        });
        return encryptedMessage
    }

    // On decrypt le message avec la formule A = B^d mob n
    decryptMessage(encryptedMessage, privateKey) {
        let decodedString = '';
        encryptedMessage.forEach((encryptedChar)=>{
            encryptedChar = bigInt(encryptedChar);
            encryptedChar = encryptedChar.modPow(privateKey.d, privateKey.n);
            decodedString = decodedString + String.fromCharCode(encryptedChar.toJSNumber());
        });
        return decodedString;
    }
}
